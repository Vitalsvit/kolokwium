from __future__ import annotations
from abc import ABC, abstractmethod


class Context:
    _state = None
    """
    A reference to the current state of the Context.
    """

    def __init__(self, state: State) -> None:
        self.transition_to(state)

    def transition_to(self, state: State):
        """
        The Context allows changing the State object at runtime.
        """

        print(f"Context: Transition to {type(state).__name__}")
        self._state = state
        self._state.context = self

    def request1(self):
        self._state.handle1()

    def request2(self):
        self._state.handle2()


class State(ABC):

    @property
    def context(self) -> Context:
        return self._context

    @context.setter
    def context(self, context: Context) -> None:
        self._context = context

    @abstractmethod
    def handle1(self) -> None:
        pass

    @abstractmethod
    def handle2(self) -> None:
        pass


# noinspection NonAsciiCharacters
class Rozpoczęty(State):
    def handle1(self) -> None:
        print("Rozpoczęty jest poprawny formalnie.")
        print("Rozpoczęty wants to change the state Oczekujący.")
        self.context.transition_to(Oczekujący())

    def handle2(self) -> None:
        print("Rozpoczęty jest braki formalne.")
        print("Rozpoczęty wants to change the state Odrzucony.")
        self.context.transition_to(Odrzucony())


class Odrzucony(State):
    def handle2(self) -> None:
        pass

    def handle1(self) -> None:
        print("Odrzucony wnioskodawca odwoła się od odrzucenia wniosku patentowego.")
        print("Odrzucony wants to change the state Odwowany.")
        self.context.transition_to(Odwowany())


class Odwowany(State):
    def handle2(self) -> None:
        pass

    def handle1(self) -> None:
        print("Odwowany odwołanie wydaje się być uzasadnione.")
        print("Odwowany wants to change the state Oczekujący.")
        self.context.transition_to(Oczekujący())


# noinspection NonAsciiCharacters
class Oczekujący(State):
    def handle1(self) -> None:
        print("Oczekujący handles Brak.")
        print("Oczekujący wants to change the state Zatwierdzony.")
        self.context.transition_to(Zatwierdzony())

    def handle2(self) -> None:
        print("Oczekujący handles już jest.")
        print("Oczekujący wants to change the state Konsultowany.")
        self.context.transition_to(Konsultowany())


class Konsultowany(State):
    def handle2(self) -> None:
        pass

    def handle1(self) -> None:
        print("Konsultowany handles biegły orzekł.")
        print("Konsultowany wants to change the state Zatwierdzony.")
        self.context.transition_to(Zatwierdzony())


class Zatwierdzony(State):
    def handle1(self) -> None:
        print("Zatwierdzony handles request1.")

    def handle2(self) -> None:
        print("Konsultowany handles już jest.")
        print("Konsultowany wants to change the state Odrzucony.")
        self.context.transition_to(Odrzucony())


if __name__ == "__main__":
    # The client code.

    context = Context(Rozpoczęty())
    context.request1()
    context.request2()
    context = Context(Odrzucony())
    context.request1()
    context.request2()
    context = Context(Odwowany())
    context.request1()
    context.request2()
    context = Context(Oczekujący())
    context.request1()
    context.request2()
    context = Context(Konsultowany())
    context.request1()
    context.request2()
    context = Context(Zatwierdzony())
    context.request1()
    context.request2()