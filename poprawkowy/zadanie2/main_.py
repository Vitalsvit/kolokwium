from __future__ import annotations
from abc import ABC, abstractmethod


class Command(ABC):
    """
    The Command interface declares a method for executing a command.
    """

    @abstractmethod
    def execute(self) -> None:
        pass


class Firma1(Command):

    def __init__(self, payload: str) -> None:
        self._payload = payload

    def execute(self) -> None:
        print(f"Akcje: See, I can do simple things like printing"
              f"({self._payload})")


class Firma2(Command):

    def __init__(self, payload: str) -> None:
        self._payload = payload

    def execute(self) -> None:
        print(f"Akcje: See, I can do simple things like printing"
              f"({self._payload})")


class Makler1(Command):
    """
    However, some commands can delegate more complex operations to other
    objects, called "receivers."
    """

    def __init__(self, receiver: Receiver, k: int, m: int) -> None:
        self._receiver = receiver
        self._k = k
        self._m = m

    k = 4.5
    m = 4.8

    def execute(self) -> None:
        """
        Commands can delegate to any methods of a receiver.
        """

        print("ComplexCommand: Complex stuff should be done by a receiver object", end="")
        self._receiver.do_something(self._k)
        self._receiver.do_something_else(self._m)


class Makler2(Command):

    def __init__(self, receiver: Receiver, k: int, m: int) -> None:
        self._receiver = receiver
        self._k = k
        self._m = m

    k = 29
    m = 31

    def execute(self) -> None:
        """
        Commands can delegate to any methods of a receiver.
        """

        print("ComplexCommand: Complex stuff should be done by a receiver object", end="")
        self._receiver.do_something(self._k)
        self._receiver.do_something_else(self._m)


class Receiver:
    """
    The Receiver classes contain some important business logic. They know how to
    perform all kinds of operations, associated with carrying out a request. In
    fact, any class may serve as a Receiver.
    """

    @staticmethod
    def do_something(a: str) -> None:
        print(f"\nReceiver: Working on ({a}.)", end="")

    @staticmethod
    def do_something_else(b: str) -> None:
        print(f"\nReceiver: Also working on ({b}.)", end="")


class Invoker:
    """
    The Invoker is associated with one or several commands. It sends a request
    to the command.
    """

    _on_start = None
    _on_finish = None

    """
    Initialize commands.
    """

    def set_on_start(self, command: Command):
        self._on_start = command

    def set_on_finish(self, command: Command):
        self._on_finish = command

    def do_something_important(self) -> None:
        """
        The Invoker does not depend on concrete command or receiver classes. The
        Invoker passes a request to a receiver indirectly, by executing a
        command.
        """

        print("Invoker: Does anybody want something done before I begin?")
        if isinstance(self._on_start, Command):
            self._on_start.execute()

        print("Invoker: ...doing something really important...")

        print("Invoker: Does anybody want something done after I finish?")
        if isinstance(self._on_finish, Command):
            self._on_finish.execute()


if __name__ == "__main__":
    """
    The client code can parameterize an invoker with any commands.
    """

    invoker = Invoker()
    invoker.set_on_start(Firma1("Firma 1 buy akcje for 5zl"))
    receiver = Receiver()
    invoker.set_on_finish(Makler1(
        receiver, "Send email", "Save report"))

    invoker.do_something_important()
    invoker = Invoker()
    invoker.set_on_start(Firma2("Firma 1 buy akcje for 30zl!"))
    receiver = Receiver()
    invoker.set_on_finish(Makler2(
        receiver, "Send email", "Save report"))
    invoker.do_something_important()
